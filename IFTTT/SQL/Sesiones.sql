create database Servidor;

use Servidor;

create table Sesiones(
	id_usuario int auto_increment not null,
    usuario varchar(100),
    clave varchar(100),
    primary key(id_usuario),
    unique key(usuario)
);

describe Sesiones;

drop table Sesiones;

insert into Sesiones values(1,'ivantm11',sha2('ivantm11',256));
insert into Sesiones (usuario, clave) values ('erikitm12',sha2('erikitm12',256));
insert into Sesiones (usuario, clave) values ('lalo', sha2('lalo',256));
insert into Sesiones (usuario, clave) values ('roy', sha2('roy',256));
insert into Sesiones (usuario, clave) values ('melissa', sha2('melissa',256));
insert into Sesiones (usuario, clave) values ('jerry', sha2('jerry',256));
insert into Sesiones (usuario, clave) values ('noe', sha2('noe',256));
insert into Sesiones (usuario, clave) values ('akatzin', sha2('akatzin',256));

use Servidor;
select * from Sesiones;

select * from Sesiones WHERE usuario='ivantm11' AND clave=sha2('ivantm11', 256);