package ProyectoSistemasDistribuidos;

public class diccionarios 
{
    static String[] dic1 = {"numC","may","min"}; 
    static String[] dic2 = {"numC","inv","may","dec2bin"};
    static String[] dic3 = {"numC","postFB","num2text"};
    static String[] puertos = {"12345","12346","12347"};
    static String[] dicGlobal = {"numC","may","min","inv","num2text","dec2bin", "postFB"};
        
    public static int isDefinedLocal(int server, String comando)
    {
        String[] dicU= {"#"};
        switch(server)
        {
            case 1: dicU = dic1;
            break;
            case 2: dicU = dic2;
            break;
            case 3: dicU = dic3;
            break;
        }
        int result=0;
        int poop = dicU.length-1;
        for(int u = 0;u <= poop;u++)
        {
            if(comando.equals(dicU[u])) result = 1;
        }
       
        return result;
    }
    
    public static int isDefinedGlobal(String comando)
    {
        int result=0;
        int poop = dicGlobal.length-1;
        for(int u = 0;u <= poop;u++)
        {
            if(comando.equals(dicGlobal[u])) result = 1;
        }
       
        return result;
    }
    
    public static String getDataConectionGlobal(String comando)
    {
        int a1=0,a2=0,result=-1;
        int poop = puertos.length;
        String[] dicU= {"#"};
        
        for(a1=1;a1 <= poop;a1++)
        {
            switch(a1)
            {
                case 1: dicU = dic1;
                break;
                case 2: dicU = dic2;
                break;
                case 3: dicU = dic3;
                break;
            }
            for(a2 = 0;a2 <= dicU.length-1;a2++)
            {
                if(comando.equals(dicU[a2])) 
                {
                    result = a1 - 1;
                    break;
                }
            }
            if(result != -1) break;
        }
        
        return puertos[result];
    }
}
