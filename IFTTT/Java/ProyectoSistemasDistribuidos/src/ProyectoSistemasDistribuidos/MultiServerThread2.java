package ProyectoSistemasDistribuidos;

import java.net.*;
import java.io.*;

public class MultiServerThread2 extends Thread
{
    private Socket socket = null;
    
    public MultiServerThread2(Socket socket) 
    {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient2.NoClients++;
    }
    
    public void run() 
    {
      try 
      {
        PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String lineIn, respuesta;
         	
        while((lineIn = entrada.readLine()) != null)
        {
            System.out.println("Received: "+lineIn);
            escritor.flush();
            
            if(lineIn.equals("FIN"))
            {
                ServerMultiClient.NoClients--;
                break;
            }
            
            String[] partes = lineIn.split("#");  
            int aux=0,i=0;
            
            if(diccionarios.isDefinedLocal(2,partes[1])==1)
            {
                switch(partes[1])
                {
                    case "numC":
                        escritor.println("#R:NoConexiones#"+ServerMultiClient2.NoClients);
                    break;
                    case "inv":
                        char[] cadena21 = partes[3].toCharArray();
                        char[] cadena22 = partes[3].toCharArray();                                
                        for(aux=cadena21.length-1;aux>=0;aux--)
                        {
                            cadena22[i]=cadena21[aux];   
                            i++;
                        }
                        escritor.println("#R:inv#"+String.valueOf(cadena22));
                    break;
                    case "may":
                        char[] cadena = partes[3].toCharArray();
                        char[] cadena2 = partes[3].toCharArray();
                        for(aux=0;aux<cadena.length;aux++)
                        {
                            cadena2[aux] = (char)(cadena[aux]-32);
                        }
                        escritor.println("#R:may#"+String.valueOf(cadena2));
                    break;
                    case "dec2bin":
                        aux = Integer.parseInt(partes[3]);
                        escritor.println("#R:dec2bin#"+Integer.toBinaryString(aux));
                    break;
                }
                aux=0;
                escritor.flush();
            }
            else
            {
                if(diccionarios.isDefinedGlobal(partes[1])==0)
                {
                    escritor.println("El comando no se encontro en ningun Server Disponible");
                }
                else
                {
                    System.out.println("Detectado en otro Servidor - "+diccionarios.getDataConectionGlobal(partes[1]));
                    String[] solicitud = {diccionarios.getDataConectionGlobal(partes[1]),lineIn};
                    respuesta = ClientFullDuplex.conexionCliente(solicitud);

                    escritor.println(respuesta);
                }
            }           
        } 
        try
        {		
            entrada.close();
            escritor.close();
            socket.close();
        }
        catch(Exception e)
        { 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	} 
      }
      catch (IOException e) 
      {
        e.printStackTrace();
      }
    }
}
