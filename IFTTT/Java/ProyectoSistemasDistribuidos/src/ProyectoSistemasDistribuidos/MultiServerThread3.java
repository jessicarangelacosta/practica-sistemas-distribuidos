package ProyectoSistemasDistribuidos;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import java.net.*;
import java.io.*;
import java.util.Scanner;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

public class MultiServerThread3 extends Thread
{
    private Socket socket = null;
    
    public MultiServerThread3(Socket socket) 
    {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient3.NoClients++;
    }
    
    public void run() 
    {
      try 
      {
        PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String lineIn, respuesta;
         	
        while((lineIn = entrada.readLine()) != null)
        {
            System.out.println("Received: "+lineIn);
            escritor.flush();
            
            if(lineIn.equals("FIN"))
            {
                ServerMultiClient.NoClients--;
                break;
            }
            
            String[] partes = lineIn.split("#");  
            int aux=0,i=0;
            
            if(diccionarios.isDefinedLocal(3,partes[1])==1)
            {
                switch(partes[1])
                {
                    case "numC":
                        escritor.println("#R:NoConexiones#"+ServerMultiClient3.NoClients);
                    break;
                    case "postFB":
                        HttpResponse re;
                        //Token de acceso se solicita al usuario desde la llamada al servicio
                        //partes[3]
                        //String accessToken = "EAAFaLvZBZC85sBAB9GnDaFrKR9XK8By73Q4AnHZCdiRZABCrgrDQ4Ph3uUZBwZC16GNkDufu3Wv9pHGTGZBXmWDHlwtaWkqLVJ0JcQaKxT2YPIAZAZBpl48d8vRjntZCDkxZAOl0KKeftKLre6oj29NxsZBq4KURGX1DGTZBNwjNxmpZA1G1sDLNkhZB7to6LzVKH5ebxHcCz7y06NIzAZDZD";
                        //Scanner Lectura = new Scanner (System.in);
                        //System.out.println("Ingrese Token de acceso:");
                        //String accessToken = Lectura.nextLine();
                        //String texto = "Prueba Graph API";
                        //Texto de publicacion igual se pide desde la llamada al servicio
                        //partes[4]
                        //System.out.println("Publicación:");
                        //String texto = Lectura.nextLine();
                        //System.out.println("Posteando...");
                        re = Request.Post("https://graph.facebook.com/v3.1/me/feed/").bodyForm(Form.form().add("message",partes[4]).add("access_token", partes[3]).build()).execute().returnResponse();
                        //System.out.println(re);
                        //System.out.println("Posteo correcto.");
                        escritor.println("#R:min#Posteo Correcto. " + re);
                    break;                    
                    case "num2text":
                        escritor.println("#R:num2text#"+numeros.cantidadConLetra(partes[3]));
                    break;
                }
                aux=0;
                escritor.flush();
            }
            else
            {
                if(diccionarios.isDefinedGlobal(partes[1])==0)
                {
                    escritor.println("El comando no se encontro en ningun Server Disponible");
                }
                else
                {
                    System.out.println("Detectado en otro Servidor - "+diccionarios.getDataConectionGlobal(partes[1]));
                    String[] solicitud = {diccionarios.getDataConectionGlobal(partes[1]),lineIn};
                    respuesta = ClientFullDuplex.conexionCliente(solicitud);

                    escritor.println(respuesta);
                }
            }           
        } 
        try
        {		
            entrada.close();
            escritor.close();
            socket.close();
        }
        catch(Exception e)
        { 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	} 
      }
      catch (IOException e) 
      {
        e.printStackTrace();
      }
    }
}
