package ProyectoSistemasDistribuidos;

import java.net.*; 
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ClientFullDuplex 
{
    public static String conexionCliente (String[] argumentos)throws IOException{ 
        Socket cliente = null; 
        PrintWriter escritor = null; 
        String DatosEnviados = null; 
        BufferedReader entrada = null;

        String maquina= "localhost",line="";
        int puerto,tipoCliente = 0;
        
        BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in));
        if (argumentos.length != 2){
            //puerto = 12345; //SERVER 1
            //puerto = 12346;  //SERVER 2
            puerto = 12347;  //SERVER 3
            System.out.println ("1-Conectado a " + maquina + " en puerto: " + puerto);
            tipoCliente=0;
        } 
        else{
            Integer pasarela = new Integer (argumentos[0]);
            puerto = pasarela.parseInt(pasarela.toString()); 
            System.out.print ("2-Conectado a " + maquina + " en puerto: " + puerto + " -- "); 
            DatosEnviados = argumentos[1]; 
            tipoCliente = 1;
        } 
        try{
                cliente = new Socket (maquina,puerto); 
        }
        catch (Exception e){
                System.out.println ("Fallo : "+ e.toString()); 
                System.exit (0); 
        }
        try{
                escritor = new PrintWriter(cliente.getOutputStream(), true);
                entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));

        }
        catch (Exception e){
                System.out.println ("Fallo : "+ e.toString()); 
                cliente.close(); 
                System.exit (0); 
        } 
        
        System.out.println("Conectado con el Servidor.");
        int sesion = 0;

        do{ 
            if(tipoCliente==0){
                //
                if(sesion == 0){
                    System.out.println("Inciar sesión en el sistema.");
                    String user = "";
                    String pass = "";
                    Scanner lecturaTeclado = new Scanner(System.in);
                    System.out.println("Usuario:    ");
                    user = lecturaTeclado.nextLine();
                    System.out.println("Password:    ");
                    pass = lecturaTeclado.nextLine();
            
                    ConectarSQL crearConexion = new ConectarSQL();
                    Connection baseSQL = crearConexion.getConection();
            
                    if(ValidarAcceso(baseSQL, user, pass) == 1){
                        System.out.println("Ingreso exitoso al sistema.");
                        crearConexion.Desconectar();
                        sesion=1;
                    }
                    else{
                        System.out.println("Error de inicio de sesión.");
                        break;
                    }
                }
                //
                DatosEnviados = DatosTeclado.readLine(); 
                escritor.println (DatosEnviados); 
                line = entrada.readLine();
                System.out.println(line);
                escritor.flush();
            }
            if(tipoCliente==1){
                System.out.println(DatosEnviados);
                escritor.println (DatosEnviados); 
                line = entrada.readLine();
                DatosEnviados = "FIN";
                escritor.flush();
            }

        }while (!DatosEnviados.equals("FIN")); 

        System.out.println ("Finalizada conexion con el servidor"); 
        try{
            escritor.close();
        }
        catch (Exception e){
        }
        tipoCliente=0;
    
        return line;
    }
        
    public static void main(String[] argumentos) throws IOException{
        String[] hola = {"hola"};
        String adios  = conexionCliente(hola);
    }
    
    public static int ValidarAcceso(Connection bd, String usuario, String clave){
        int resultado = 0;
        String SSQL = "select * from Sesiones WHERE usuario='"+usuario+"' AND clave=sha2('"+clave+"',256)";
        try{
            Statement st = bd.createStatement();
            ResultSet rs = st.executeQuery(SSQL);
            if(rs.next()){
                resultado=1;
            }
        } catch (SQLException ex) {
            System.out.println("Error de conexión\n" + ex);
        }
        
        return resultado;
    }
}
