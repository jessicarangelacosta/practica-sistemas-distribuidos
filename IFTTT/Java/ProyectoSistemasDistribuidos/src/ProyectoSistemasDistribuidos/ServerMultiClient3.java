package ProyectoSistemasDistribuidos;

import java.io.*;
import java.net.*;

public class ServerMultiClient3 
{
    static int NoClients=0;
    
    public static void main (String[] argumentos)throws IOException
    {
	ServerSocket socketServidor = null;
	Socket socketCliente = null;
        int enproceso=1;
        
	try
        {
	   socketServidor = new ServerSocket (12347);
	}
        catch (Exception e)
        {
	   System.out.println ("Error : "+ e.toString());
	   System.exit (0);
	}

	System.out.println ("Server started... (Socket TCP)");
	
	while(enproceso==1){
		try
                {
                    socketCliente = socketServidor.accept();
                    MultiServerThread3 controlThread=new MultiServerThread3(socketCliente);
                    controlThread.start();
	   	}
                catch (Exception e)
                {
                    System.out.println ("Error : " + e.toString());
                    socketServidor.close();
                    System.exit (0);
	   	}
	}
	System.out.println("Finalizando Servidor...");
   }
}
