package ProyectoSistemasDistribuidos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ivantrejo
 */
public class ConectarSQL {
    private static Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "root";
    private static final String password  = "ivantm11";
    private static final String url = "jdbc:mysql://localhost:3306/Servidor?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    
    public ConectarSQL(){
        conn = null;
        try{
            //Class.forName(driver);
            conn = DriverManager.getConnection(url,user,password);
            if(conn != null){
                //System.out.println("Conexión establecida con MySQL correctamente.");
            }
        }
        catch(SQLException e){
            System.out.println("Error al conectar con MySQL.\n"+ e);
        }
    }
    
    public Connection getConection(){
        return conn;
    }
    
    public void Desconectar(){
        conn = null;
        if(conn == null){
            //System.out.println("Desconectado de MySQL correctamente.");
        }
    }
}
