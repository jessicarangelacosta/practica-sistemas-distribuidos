package ProyectoSistemasDistribuidos;

import java.net.*;
import java.io.*;

public class MultiServerThread extends Thread{
    private Socket socket = null;
    
    public MultiServerThread(Socket socket){
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }
    
    public void run(){
      try{
        PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String lineIn, respuesta;
         	
        while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            
            if(lineIn.equals("FIN")){
                ServerMultiClient.NoClients--;
                break;
            }
            
            String[] partes = lineIn.split("#");  
            int aux=0;
            
            if(diccionarios.isDefinedLocal(1,partes[1])==1){
                switch(partes[1]){
                    case "numC":
                        escritor.println("#R:NoConexiones#"+ServerMultiClient.NoClients);
                    break;
                    case "may":
                        char[] cadena = partes[3].toCharArray();
                        char[] cadena2 = partes[3].toCharArray();
                        for(aux=0;aux<cadena.length;aux++){
                            cadena2[aux] = (char)(cadena[aux]-32);
                        }
                        escritor.println("#R:may#"+String.valueOf(cadena2));
                    break;
                    case "min":
                        char[] cadena12 = partes[3].toCharArray();
                        char[] cadena22 = partes[3].toCharArray();
                        for(aux=0;aux<cadena12.length;aux++){
                           cadena22[aux] = (char)(cadena12[aux]+32);
                        }
                        escritor.println("#R:min#"+String.valueOf(cadena22));
                    break;
                }
                aux=0;
                escritor.flush();
            }
            else{
                if(diccionarios.isDefinedGlobal(partes[1])==0){
                    escritor.println("El comando no se encontro en ningun Server Disponible");
                }
                else{
                    System.out.println("Detectado en otro Servidor - "+diccionarios.getDataConectionGlobal(partes[1]));
                    String[] solicitud = {diccionarios.getDataConectionGlobal(partes[1]),lineIn};
                    respuesta = ClientFullDuplex.conexionCliente(solicitud);

                    escritor.println(respuesta);
                }
            }           
        } 
        try{		
            entrada.close();
            escritor.close();
            socket.close();
        }
        catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	} 
      }
      catch (IOException e){
        e.printStackTrace();
      }
    }
}
