import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.Scanner;
import servidoressistdistr.temp;

public class MultiServerThread extends Thread {
    
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {
      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	    while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
			      break;
			   } 
            //PETICIÓN #CUANTOS
            else if (lineIn.equals("#cuantos#-request")){
                escritor.println("NoClientes..."+ServerMultiClient.NoClients);
            }
            //PETICIÓN #M
            else if (lineIn.equals("#M")){
                escritor.println("#M#"+lineIn.toUpperCase()+"#-request");
            }
            //PETICIÓN #m
            else if (lineIn.equals("#m")){
                escritor.println("#m#"+lineIn.toLowerCase()+"#-request");
            }
            //PETICIÓN #I
            else if (lineIn.equals("#I")){
             String cadena=lineIn;
             char []invertir=cadena.toCharArray();
             int cont;
                for(cont=cadena.length()-1;cont>=0;cont--){
                    System.out.print("#I#"+invertir[cont]);
            }
            }
            //PETICIÓN #CRYPT/DECRYPT
            else if (lineIn.equals("#crypt")){
                String mensaje = lineIn;
                char array[] = mensaje.toCharArray();
                for(int i=0; i<array.length;i++){
                    array[i]=(char)(array[i]+(char)5);
                }
                String encriptado = String.valueOf(array);
                System.out.println("#crypt#" + encriptado);
            }
            //PETICIÓN #DECRYPT
            else if (lineIn.equals("#decrypt")){
                String mensaje = lineIn;
                char array[] = mensaje.toCharArray();
                for(int i=0; i<array.length;i++){
                    array[i]=(char)(array[i]+(char)5);
                }
                String encriptado = String.valueOf(array);
                System.out.println("#crypt#" + encriptado);
                
                char arrayD[] = encriptado.toCharArray();
                for(int i = 0; i<arrayD.length;i++){
                    arrayD[i]=(char)(arrayD[i]);
                }
                String desencriptado = String.valueOf(arrayD);
                System.out.println("#decrypt#" + desencriptado);
            }            
            //PETICIÓN #B
            else if (lineIn.equals("#B")){
                Scanner leer = new Scanner(System.in);
                int decimal,modulo;
                String binario = "";
                System.out.println("Numero Decimal: ");
                decimal = leer.nextInt();
                while(decimal>0){
                    modulo = (decimal%2);
                    binario = modulo + binario;
                    decimal = decimal/2;
                }
                System.out.println("#B#" + binario);
            }
            //PETICIÓN TEMPERATURA Y HUMEDAD RELATIVA
            else if(lineIn.equals("#temp")){
                temp r = new temp();
                r.openFile();
                r.readFile();
                r.closeFile();
            }            
            else{
               escritor.println("Echo... "+lineIn);
               escritor.flush();
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
//@humbergas